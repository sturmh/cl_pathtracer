#include "App.h"
#include "GLHelpers.h"
#include "CLHelpers.h"
#include "cl.hpp"
#include <chrono>
using namespace std::chrono;

int width = 400;
int height = 300;
GLint program = -1;
GLuint gl_frame_buffer_tex;
GLuint vao, vbo;

struct Camera {
    int viewport_width;
    int viewport_height;

    float camera_near_clip;
    float camera_far_clip;

    float camera_tan_fov_x_div_two; 
    float camera_tan_fov_y_div_two;

    float camera_right[3];
    float camera_up[3];
    float camera_look[3];
    float camera_pos[3];
};

void HandleInput(const app::KeyState& key_state, const app::CursorState& cursor_state, float dt) {
}

void App::OnStart() {    
    LOG_D("GL_VERSION: %s", glGetString(GL_VERSION));
    LOG_D("GL_SHADING_LANGUAGE_VERSION: %s", glGetString(GL_SHADING_LANGUAGE_VERSION));
    LOG_D("GL_VENDOR: %s", glGetString(GL_VENDOR));
    LOG_D("GL_RENDERER: %s", glGetString(GL_RENDERER));
    glClearColor(0.1f, 0.1f, 0.1f, 1.f);    
    //glEnable (GL_DEPTH_TEST);    
    glEnable(GL_CULL_FACE);
    glFrontFace(GL_CCW);
    glCullFace(GL_BACK);
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    //glPolygonMode(GL_FRONT_AND_BACK, GL_LINE); 

    GLuint shaders[2];
    shaders[0] = gl::CreateShaderFromFile(GL_VERTEX_SHADER, "/Users/eugene.sturm/projects/misc/opencl_pathtracer/fsq_vs.glsl");
    shaders[1] = gl::CreateShaderFromFile(GL_FRAGMENT_SHADER, "/Users/eugene.sturm/projects/misc/opencl_pathtracer/fsq_ps.glsl");
    program = gl::CreateProgram(shaders, 2);
    
    GL_CHECK(glActiveTexture(GL_TEXTURE0));
    GL_CHECK(glGenTextures(1, &gl_frame_buffer_tex));
    GL_CHECK(glBindTexture(GL_TEXTURE_2D, gl_frame_buffer_tex));
    GL_CHECK(glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, GL_RGB, GL_FLOAT, NULL));
    GL_CHECK(glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST));
    GL_CHECK(glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST));



     //get all platforms (drivers)
    std::vector<cl::Platform> all_platforms;
    cl::Platform::get(&all_platforms);
    if(all_platforms.size()==0){
        std::cout<<" No platforms found. Check OpenCL installation!\n";
        exit(1);
    }
    cl::Platform default_platform=all_platforms[0];
    std::cout << "Using platform: "<<default_platform.getInfo<CL_PLATFORM_NAME>()<<"\n";
 
    //get default device of the default platform
    std::vector<cl::Device> all_devices;
    default_platform.getDevices(CL_DEVICE_TYPE_ALL, &all_devices);
    if(all_devices.size()==0){
        std::cout<<" No devices found. Check OpenCL installation!\n";
        exit(1);
    }
    cl::Device default_device = all_devices[1]; // have to use device 1 instaed of 2...event thought both say they support images
    std::cout << "Image support? " << (default_device.getInfo<CL_DEVICE_IMAGE_SUPPORT >() == CL_TRUE ? "YES" : "NO") << "\n";
    std::cout << "Using device: "<<default_device.getInfo<CL_DEVICE_NAME>()<<"\n";
 
    std::vector<cl_context_properties> props = GetSharedContextProperties();
    cl::Context context({default_device}, props.data());
 
    cl::Program::Sources sources;

    // generate_camera_rays
    // [while rays to shoot]
    //  intersect rays with scene and generate new rays
    //  accumulate color


    cl::Program program = BuildProgramFromFiles(context, default_device, {"/Users/eugene.sturm/projects/misc/opencl_pathtracer/generate_rays.cl.cpp"});    


    // create buffers on the device    
    size_t ray_buffer_len = sizeof(float) * 3 * width * height;    
    cl::Buffer ray_buffer(context, CL_MEM_READ_WRITE, ray_buffer_len); 
    cl::Buffer cam_right(context, CL_MEM_READ_WRITE, sizeof(float)*3);
    cl::Buffer cam_up(context, CL_MEM_READ_WRITE, sizeof(float)*3);
    cl::Buffer cam_look(context, CL_MEM_READ_WRITE, sizeof(float)*3);    
    cl::Buffer cam_pos(context, CL_MEM_READ_WRITE, sizeof(float)*3); 
    cl_int err = 0;
    cl::ImageGL gl_image(context, CL_MEM_READ_WRITE, GL_TEXTURE_2D, 0, gl_frame_buffer_tex
    , &err);    
    std::cout << "Err: " << err << std::endl;
    
     
 
    //create queue to which we will push commands for the device.
    cl::CommandQueue queue(context,default_device);
    std::vector<cl::Memory> gl_objects = { gl_image };
    err = queue.enqueueAcquireGLObjects(&gl_objects); 
    if(err != CL_SUCCESS) {
        std::cout << "Failed to aquire gl objects " << GetCLErrorString(err) << "\n";
    }
    //write arrays A and B to the device
    float camera_right[3] = { 1, 0, 0 };
    float camera_up[3] = { 0, 1, 0 };
    float camera_look[3] = { 0, 0, 1 };
    float camera_pos[3] = { 0, 0, 0 };

    queue.enqueueWriteBuffer(cam_right,CL_TRUE,0,sizeof(float)*3, camera_right);
    queue.enqueueWriteBuffer(cam_up,CL_TRUE,0,sizeof(float)*3, camera_up);
    queue.enqueueWriteBuffer(cam_look,CL_TRUE,0,sizeof(float)*3, camera_look);
    queue.enqueueWriteBuffer(cam_pos,CL_TRUE,0,sizeof(float)*3, camera_pos);
    
    
    
    cl::make_kernel<uint64_t, cl::Buffer&, int, int, int, float, float, float, float, cl::Buffer&, cl::Buffer&, cl::Buffer&> GenerateCameraRays(program, "GenerateCameraRays");
    cl::make_kernel<cl::Buffer&, int, cl::ImageGL&, cl::ImageGL&> IntersectScene(program, "IntersectScene");
    cl::make_kernel<uint64_t, cl::ImageGL&, cl::ImageGL&, int, int, float, float, float, float, cl::Buffer&, cl::Buffer&, cl::Buffer&, cl::Buffer&> TraceRays(program, "TraceRays");
    cl::EnqueueArgs generate_camera_rays_eargs(queue, cl::NullRange ,cl::NDRange(width, height), cl::NullRange);
    cl::EnqueueArgs intersect_scene_eargs(queue, cl::NullRange ,cl::NDRange(width, height), cl::NullRange);
    uint64_t frame_time = duration_cast< milliseconds >(system_clock::now().time_since_epoch()).count();
    std::cout << frame_time << std::endl;
    //GenerateCameraRays(generate_camera_rays_eargs, frame_time,ray_buffer, ray_buffer_len, width, height, 0.1f, 1000.f, tan(3.14f / 2.f / 2.f), tan(3.14f / 2.f / (4.f / 3.f) / 2.f), cam_right, cam_up, cam_look).wait();
    //IntersectScene(intersect_scene_eargs, ray_buffer, ray_buffer_len, gl_image, gl_image).wait();
    TraceRays(generate_camera_rays_eargs, 
        frame_time, 
        gl_image, 
        gl_image, 
        width, 
        height, 
        0.1f, 
        1000.f, 
        tan(3.14f / 2.f / 2.f), 
        tan(3.14f / 2.f / (4.f / 3.f) / 2.f), 
        cam_right, 
        cam_up, 
        cam_look, 
        cam_pos)
    .wait();
    
    /*
    float* local_ray_buffer = new float[3*width*height];
     queue.enqueueReadBuffer(ray_buffer, CL_TRUE, 0, ray_buffer_len, local_ray_buffer);
    std::cout << std::endl;
    for(int i = 0; i < height; ++i) {
        for(int j = 0; j < width; ++j) {
            int index = (j + i * width) * 3;
            std::cout << i << "," << j << ": "<< local_ray_buffer[index] << ", " << local_ray_buffer[index + 1] << ", " << local_ray_buffer[index + 2] << std::endl;            
        }
    }
*/

   /* float local_buff[3*400*300];
    memset(local_buff, 0, sizeof(local_buff));
    
    cl::size_t<3> o;
    o[0] = 0;
    o[1] = 0;
    o[2] = 0;

    cl::size_t<3> r;
    r[0] = width;
    r[1] = height;
    r[2] = 1;
    
    int C[10];
    float* local_ray_buffer = new float[3*width*height];
    //read result C from the device to array C
    queue.enqueueReadBuffer(ray_buffer, CL_TRUE, 0, ray_buffer_len, local_ray_buffer);
    std::cout << std::endl;
    for(int i = 0; i < height; ++i) {
        for(int j = 0; j < width; ++j) {
            int index = (j + i * width) * 3;
            //std::cout << i << "," << j << ": "<< local_ray_buffer[index] << ", " << local_ray_buffer[index + 1] << ", " << local_ray_buffer[index + 2] << std::endl;            
        }
    }

      // Copy contiguous buffer to formatted image bound to GL texture
    //err = queue.enqueueWriteImage(gl_image, CL_TRUE, o, r, 0, 0, local_ray_buffer);
    //if(err != CL_SUCCESS) {
      //  std::cout << "Failed to write to gl image " << GetCLErrorString(err) << std::endl;
   // }
*/
    queue.enqueueReleaseGLObjects(&gl_objects);
    cl::finish();    
    

    // for fullscreen quad
    GL_CHECK(glGenVertexArrays(1, &vao));
    GL_CHECK(glBindVertexArray(vao));
    GL_CHECK(glGenBuffers(1, &vbo));
    GL_CHECK(glBindBuffer(GL_ARRAY_BUFFER, vbo));
    GL_CHECK(glBufferData(GL_ARRAY_BUFFER, 0, NULL, GL_STATIC_DRAW));    

}

void App::OnFrame(const app::AppState* app_state, float dt) {    
    HandleInput(app_state->key_state, app_state->cursor_state, dt);
    
    GL_CHECK(glClear(GL_COLOR_BUFFER_BIT));

    GL_CHECK(glBindVertexArray(vao));        
    GL_CHECK(glUseProgram(program));            
    GL_CHECK(glDrawArrays(GL_TRIANGLE_STRIP, 0, 3));
    
}
void App::OnShutdown() {

}

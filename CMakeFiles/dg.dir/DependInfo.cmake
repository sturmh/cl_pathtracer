# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/Users/eugene.sturm/projects/misc/opencl_pathtracer/App.cpp" "/Users/eugene.sturm/projects/misc/opencl_pathtracer/CMakeFiles/dg.dir/App.cpp.o"
  "/Users/eugene.sturm/projects/misc/opencl_pathtracer/Camera.cpp" "/Users/eugene.sturm/projects/misc/opencl_pathtracer/CMakeFiles/dg.dir/Camera.cpp.o"
  "/Users/eugene.sturm/projects/misc/opencl_pathtracer/SystemGLFW3.cpp" "/Users/eugene.sturm/projects/misc/opencl_pathtracer/CMakeFiles/dg.dir/SystemGLFW3.cpp.o"
  "/Users/eugene.sturm/projects/misc/opencl_pathtracer/main.cpp" "/Users/eugene.sturm/projects/misc/opencl_pathtracer/CMakeFiles/dg.dir/main.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "Clang")

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# The include file search paths:
set(CMAKE_C_TARGET_INCLUDE_PATH
  "/usr/local/include"
  )
set(CMAKE_CXX_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
set(CMAKE_Fortran_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
set(CMAKE_ASM_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})

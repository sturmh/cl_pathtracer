#include "Application.h"

namespace sys {
    void SetWindowTitle(const char* title);
    float GetTime();
    int Run(app::Application* app);
}
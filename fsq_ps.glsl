#version 410 core

in vec2 tex_coord;
out vec4 frag_color;

uniform sampler2D frame_buffer;

void main() {    
    vec3 diffuse = texture(frame_buffer, tex_coord).xyz;
    frag_color = vec4(diffuse, 1.0);
    
}
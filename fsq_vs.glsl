#version 410 core

out vec2 tex_coord;

/**
 Build right triangle x2 the length and height of the frame
 ID=0 -> Pos=[-1,-1], Tex=(0,0)
 ID=1 -> Pos=[-1, 3], Tex=(0,2)
 ID=2 -> Pos=[ 3,-1], Tex=(2,0)
 **/

void main() {
    tex_coord.x = (gl_VertexID == 2) ?  2.0 :  0.0;
    tex_coord.y = (gl_VertexID == 1) ?  2.0 :  0.0;
    gl_Position = vec4(tex_coord * vec2(2.0, -2.0) + vec2(-1.0, 1.0), 1.0, 1.0);
}
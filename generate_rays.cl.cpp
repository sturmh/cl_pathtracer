
// Thanks Java
ulong rand(ulong* seed, uint bits) {
    *seed = (*seed * 0x5DEECE66DL + 0xBL) & ((1L << bits) - 1);    
    return *seed;
}

// Thanks Java
float rand_uniform(ulong* seed) {   
    return (float)(rand(seed, 24)) / (float)(1 << 24);
}

uint wang_hash32(uint seed) {
    seed = (seed ^ 61) ^ (seed >> 16);
    seed *= 9;
    seed = seed ^ (seed >> 4);
    seed *= 0x27d4eb2d;
    seed = seed ^ (seed >> 15);
    return seed;
}

ulong wang_hash64(ulong key) {
  key = (~key) + (key << 21); // key = (key << 21) - key - 1;
  key = key ^ (key >> 24);
  key = (key + (key << 3)) + (key << 8); // key * 265
  key = key ^ (key >> 14);
  key = (key + (key << 2)) + (key << 4); // key * 21
  key = key ^ (key >> 28);
  key = key + (key << 31);
  return key;
}

__kernel 
void TraceRays(
    ulong frame_time,
    __read_only  image2d_t read_frame,
    __write_only image2d_t write_frame,
    int viewport_width, 
    int viewport_height,
    float camera_near_clip, 
    float camera_far_clip, 
    float camera_tan_fov_x_div_two, 
    float camera_tan_fov_y_div_two, 
    __global float* camera_right, 
    __global float* camera_up, 
    __global float* camera_look,
    __global float* camera_pos) 
{   

    const sampler_t smp =   CLK_NORMALIZED_COORDS_FALSE | //Natural coordinates
                            CLK_ADDRESS_CLAMP | //Clamp to zeros
                            CLK_FILTER_NEAREST; //Don't interpolate

    int2 pixel_coord = (int2)(
        get_global_id(0),
        get_global_id(1)
    );    
    
    int offset = (pixel_coord.s0 + pixel_coord.s1 * viewport_width) * 3;
    int global_thread_id = offset;
    ulong seed = wang_hash64(frame_time + global_thread_id);        
    
    float3 rd = (float3)(
        (((pixel_coord.s0 + 0.5f) / viewport_width) * 2.f - 1.f) * camera_tan_fov_x_div_two,
        (((pixel_coord.s1 + 0.5f) / viewport_height) * 2.f - 1.f) * camera_tan_fov_y_div_two * -1.f,
        camera_near_clip
    );
    
    float3 ro = (float3)(camera_pos[0], camera_pos[1], camera_pos[2]);
    rd = normalize((float3)(
        rd.s0 * camera_right[0] + rd.s1 * camera_up[0] + rd.s2 * camera_look[0],
        rd.s0 * camera_right[1] + rd.s1 * camera_up[1] + rd.s2 * camera_look[1],
        rd.s0 * camera_right[2] + rd.s1 * camera_up[2] + rd.s2 * camera_look[2]
    ));

    float4 color = (float4)(
    (rd.s0 + 1.f) / 2.f,//     rand_uniform(&seed),
    (rd.s1 + 1.f) / 2.f,//     rand_uniform(&seed),
    (rd.s2 + 1.f) / 2.f,//     rand_uniform(&seed),
        0
    );    
    /*
    float4 val = read_imagef(read_frame, smp, pixel_coord);
    color = (float4)(
        val.s0/2.f,
        0,
        0,
        0
    );*/  
    write_imagef(write_frame, pixel_coord, color);  
    
   
}

__kernel 
void GenerateCameraRays(
    ulong frame_time,
    __global float* ray_buffer, 
    int ray_buffer_size, 
    int viewport_width, 
    int viewport_height,
    float camera_near_clip, 
    float camera_far_clip, 
    float camera_tan_fov_x_div_two, 
    float camera_tan_fov_y_div_two, 
    __global float* camera_right, 
    __global float* camera_up, 
    __global float* camera_look) 
{ 
    
    int pixel_x = get_global_id(0); 
    int pixel_y = get_global_id(1); 
    int offset = (pixel_x + pixel_y * viewport_width) * 3;
    int global_thread_id = offset;
    ulong seed = wang_hash64(frame_time + global_thread_id);        
    
    float3 rd = (float3)(
        (((pixel_x + 0.5f) / viewport_width) * 2.f - 1.f) * camera_tan_fov_x_div_two,
        (((pixel_y + 0.5f) / viewport_height) * 2.f - 1.f) * camera_tan_fov_y_div_two * -1.f,
        camera_near_clip
    );
    
    rd = normalize((float3)(
        rd.s0 * camera_right[0] + rd.s1 * camera_up[0] + rd.s2 * camera_look[0],
        rd.s0 * camera_right[1] + rd.s1 * camera_up[1] + rd.s2 * camera_look[1],
        rd.s0 * camera_right[2] + rd.s1 * camera_up[2] + rd.s2 * camera_look[2]
    ));

    
    ray_buffer[offset] = rand_uniform(&seed);;
    ray_buffer[offset+1] = rand_uniform(&seed);;
    ray_buffer[offset+2] = rand_uniform(&seed);;    
}

__kernel
void IntersectScene( 
    __global float* ray_buffer, 
    int ray_buffer_len,    
    __read_only  image2d_t read_frame,
    __write_only image2d_t write_frame) 
{    
    const sampler_t smp =   CLK_NORMALIZED_COORDS_FALSE | //Natural coordinates
                            CLK_ADDRESS_CLAMP | //Clamp to zeros
                            CLK_FILTER_NEAREST; //Don't interpolate
    int global_id = 0;
    int pixel_x = get_global_id(0); 
    int pixel_y = get_global_id(1); 
    int offset = (pixel_x + pixel_y * 400) * 3;

    int2 coord = (int2)(
        get_global_id(0),
        get_global_id(1)
    );

    float4 rgba = (float4)(
        ray_buffer[offset],
        ray_buffer[offset+1],
        ray_buffer[offset+2],
        0
    );
        
    write_imagef(write_frame, coord, rgba);
}